Banking System
==============

A command line app for managing the personal bank account system.

Author: <strong>AGUEDO Claudio</strong>

### Context
Think of your personal bank account experience. When in doubt, go for the simplest solution.

#### Requirements:
- Deposit and Withdrawal
- Account statement (date, amount, balance)
- Statement printing

#### User Stories

US 1:
+ In order to save money
+ As a bank client
+ I want to make a deposit in my account

US 2:
+ In order to retrieve some or all of my savings
+ As a bank client
+ I want to make a withdrawal from my account

US 3:
+ In order to check my operations
+ As a bank client
+ I want to see the history (operation, date, amount, balance) of my operations

### Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#### Prerequisites
What things you need to install the software and how to install them

+ Java 1.8
+ Maven 3.6.2

#### Installing
A step by step series of examples that tell you how to get a development env running:

1.Install Java:

https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html


2.Install Maven:

https://maven.apache.org/install.html

#### Running App
Post installation of Java and Maven, you will be ready to import the code into an IDE of your choice and run the Main.java class to launch the application.

#### Testing
The app has unit tests written.

To run the tests from maven, go to the folder that contains the pom.xml file and execute the below command.

<blockquote>
    mvn test
</blockquote>