package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.domain.Operation;
import fr.bank.bankingsystem.service.AccountStatementHistoricalPrinter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAccountStatementHistoricalConsolePrinterImpl {
    private static AccountStatementHistoricalPrinter accountStatementHistoricalPrinter;
    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final PrintStream originalOut = System.out;


    @BeforeAll
    private static void setUp() {
        System.setOut(new PrintStream(outContent));
        accountStatementHistoricalPrinter = AccountStatementHistoricalConsolePrinter.getInstance();
    }

    @AfterAll
    private static void restoreStream() {
        System.setOut(originalOut);
    }

    @Test
    public void should_print_historical_client_account_statements_if_client_check_operations() {
        Account account = new Account();
        List<AccountStatement> accountStatementList = new ArrayList<>();
        accountStatementList.add(new AccountStatement(Operation.DEPOSIT, LocalDateTime.now(), BigDecimal.valueOf(20), BigDecimal.valueOf(20)));

        account.setAccountStatementList(accountStatementList);

        accountStatementHistoricalPrinter.print(account);
        assertEquals(accountStatementList.get(0).toString(), outContent.toString().trim());
    }
}
