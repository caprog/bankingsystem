package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.domain.Operation;
import fr.bank.bankingsystem.service.AccountOperation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDepositOperationImpl {
    private static AccountOperation accountOperation;

    @BeforeAll
    private static void setUp() {
        accountOperation = DepositOperationImpl.getInstance();
    }

    @Test
    public void should_deposit_in_client_account_if_client_save_money() {
        Account account = new Account();
        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());
        assertEquals(accountStatement.getOperation(), Operation.DEPOSIT);
    }

    @Test
    public void should_deposit_100_eur_in_client_account_if_client_save_100_eur() {
        Account account = new Account();
        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());
        assertEquals(accountStatement.getAmount(), BigDecimal.valueOf(100));
    }

    @Test
    public void should_add_100_eur_to_account_client_balance_if_client_save_100_eur() {
        Account account = new Account();
        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());
        assertEquals(accountStatement.getBalance(), BigDecimal.valueOf(100));
    }
}
