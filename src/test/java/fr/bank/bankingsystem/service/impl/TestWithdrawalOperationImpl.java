package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.domain.Operation;
import fr.bank.bankingsystem.service.AccountOperation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestWithdrawalOperationImpl {
    private static AccountOperation accountOperation;

    @BeforeAll
    private static void setUp() {
        accountOperation = WithdrawalOperationImpl.getInstance();
    }

    @Test
    public void should_withdrawal_in_client_account_if_client_retrieve_money() {
        Account account = new Account();
        List<AccountStatement> accountStatementList = new ArrayList<>();
        accountStatementList.add(new AccountStatement(Operation.DEPOSIT, LocalDateTime.now(), BigDecimal.valueOf(150), BigDecimal.valueOf(150)));
        account.setAccountStatementList(accountStatementList);

        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());

        assertEquals(accountStatement.getOperation(), Operation.WITHDRAWAL);
    }

    @Test
    public void should_withdrawal_100_eur_in_client_account_if_client_retrieve_100_eur() {
        Account account = new Account();
        List<AccountStatement> accountStatementList = new ArrayList<>();
        accountStatementList.add(new AccountStatement(Operation.DEPOSIT, LocalDateTime.now(), BigDecimal.valueOf(150), BigDecimal.valueOf(150)));
        account.setAccountStatementList(accountStatementList);

        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());

        assertEquals(accountStatement.getAmount(), BigDecimal.valueOf(100));
    }

    @Test
    public void should_subtract_100_eur_to_account_client_balance_if_client_retrieve_100_eur() {
        Account account = new Account();
        List<AccountStatement> accountStatementList = new ArrayList<>();
        accountStatementList.add(new AccountStatement(Operation.DEPOSIT, LocalDateTime.now(), BigDecimal.valueOf(150), BigDecimal.valueOf(150)));
        account.setAccountStatementList(accountStatementList);

        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(100), LocalDateTime.now());

        assertEquals(accountStatement.getBalance(), BigDecimal.valueOf(50));
    }
}
