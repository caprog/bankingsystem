package fr.bank.bankingsystem.domain;

public enum Operation {
    DEPOSIT, WITHDRAWAL
}
