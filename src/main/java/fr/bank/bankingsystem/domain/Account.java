package fr.bank.bankingsystem.domain;

import java.util.List;

public class Account {
    private List<AccountStatement> accountStatementList;

    public List<AccountStatement> getAccountStatementList() {
        return accountStatementList;
    }

    public void setAccountStatementList(List<AccountStatement> accountStatementList) {
        this.accountStatementList = accountStatementList;
    }
}
