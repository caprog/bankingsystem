package fr.bank.bankingsystem.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AccountStatement {
    private Operation operation;
    private LocalDateTime date;
    private BigDecimal amount;
    private BigDecimal balance;
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss SSS");

    public AccountStatement() {}

    public AccountStatement(Operation operation, LocalDateTime date, BigDecimal amount, BigDecimal balance) {
        this.operation = operation;
        this.date = date;
        this.amount = amount;
        this.balance = balance;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public static DateTimeFormatter getDateTimeFormatter() {
        return DATE_TIME_FORMATTER;
    }

    @Override
    public String toString() {
        return "AccountStatement{" +
                "operation=" + operation +
                ", date=" + DATE_TIME_FORMATTER.format(date) +
                ", amount=" + amount +
                ", balance=" + balance +
                '}';
    }
}
