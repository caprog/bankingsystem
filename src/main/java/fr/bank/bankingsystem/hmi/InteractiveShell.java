package fr.bank.bankingsystem.hmi;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.hmi.actions.AccountActionShell;
import fr.bank.bankingsystem.hmi.actions.AccountStatementHistoricalActionShell;
import fr.bank.bankingsystem.hmi.actions.DepositAccountActionShell;
import fr.bank.bankingsystem.hmi.actions.WithdrawalAccountActionShell;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class InteractiveShell {
    private static final List<Pair<String, AccountActionShell>> MAIN_MENU_LIST;

    static{
        MAIN_MENU_LIST = new ArrayList<>();
        MAIN_MENU_LIST.add(new Pair<>("Deposit", new DepositAccountActionShell()));
        MAIN_MENU_LIST.add(new Pair<>("Withdrawal", new WithdrawalAccountActionShell()));
        MAIN_MENU_LIST.add(new Pair<>("Statement Printing", new AccountStatementHistoricalActionShell()));
        MAIN_MENU_LIST.add(new Pair<>("Exit", (account) -> false));
    }

    private static InteractiveShell INSTANCE;

    private InteractiveShell() {}

    public static synchronized InteractiveShell getInstance() {
        if(INSTANCE == null)
            INSTANCE = new InteractiveShell();

        return INSTANCE;
    }

    public void start() {
        Account account = new Account();
        account.setAccountStatementList(new ArrayList<>());

        boolean next = true;
        final int menuSize = MAIN_MENU_LIST.size();
        while(next) {

            System.out.println("Personal account");
            printMenuList(MAIN_MENU_LIST);

            System.out.println("Choose an option number");
            int option = ScannerUtils.nextInt(
                (unused) -> System.out.println("Invalid option number"),
                (value) -> checkValidOptionMenu(value, menuSize)
            );

            AccountActionShell action = MAIN_MENU_LIST.get(option - 1).getValue();
            next = action.perform(account);
            System.out.println();
        }
    }

    private void printMenuList(List<Pair<String, AccountActionShell>> menuList) {
        // Print option menu list
        IntStream.range(0, menuList.size())
                .forEach(i -> System.out.println((i+1) + "." + menuList.get(i).getKey()));
    }

    private boolean checkValidOptionMenu(int optionNumber, int menuSize) {
        boolean valid = 1 <= optionNumber && optionNumber <= menuSize;
        if(!valid)
            System.out.println("Invalid option number");
        return valid;
    }
}
