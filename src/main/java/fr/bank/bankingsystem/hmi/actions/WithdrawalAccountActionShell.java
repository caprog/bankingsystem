package fr.bank.bankingsystem.hmi.actions;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.hmi.ScannerUtils;
import fr.bank.bankingsystem.service.AccountOperation;
import fr.bank.bankingsystem.service.impl.WithdrawalOperationImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class WithdrawalAccountActionShell implements AccountActionShell {
    private AccountOperation accountOperation = WithdrawalOperationImpl.getInstance();

    @Override
    public boolean perform(Account account) {
        System.out.println("Withdrawal");
        System.out.println("Enter the amount:");
        int amount = ScannerUtils.nextInt(
            (unused) ->  System.out.println("Invalid amount ")
        );

        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(amount), LocalDateTime.now());
        if(accountStatement != null)
            account.getAccountStatementList().add(accountStatement);

        return true;
    }
}
