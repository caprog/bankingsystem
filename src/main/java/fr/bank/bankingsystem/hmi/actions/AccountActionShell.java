package fr.bank.bankingsystem.hmi.actions;

import fr.bank.bankingsystem.domain.Account;

public interface AccountActionShell {
    boolean perform(Account account);
}
