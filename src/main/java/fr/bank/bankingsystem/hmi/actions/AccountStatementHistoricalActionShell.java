package fr.bank.bankingsystem.hmi.actions;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.service.AccountStatementHistoricalPrinter;
import fr.bank.bankingsystem.service.impl.AccountStatementHistoricalConsolePrinter;

public class AccountStatementHistoricalActionShell implements AccountActionShell{
    private AccountStatementHistoricalPrinter accountStatementHistoricalPrinter = AccountStatementHistoricalConsolePrinter.getInstance();

    @Override
    public boolean perform(Account account) {
        System.out.println("Statement printing");
        if(account.getAccountStatementList().size() == 0)
            System.out.println("Historical operation is empty");
        else
            accountStatementHistoricalPrinter.print(account);
        return true;
    }
}
