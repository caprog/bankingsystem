package fr.bank.bankingsystem.hmi.actions;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.hmi.ScannerUtils;
import fr.bank.bankingsystem.service.AccountOperation;
import fr.bank.bankingsystem.service.impl.DepositOperationImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class DepositAccountActionShell implements AccountActionShell {
    private AccountOperation accountOperation = DepositOperationImpl.getInstance();

    @Override
    public boolean perform(Account account) {
        System.out.println("Deposit");
        System.out.println("Enter the amount:");
        int amount = ScannerUtils.nextInt(
            (unused) ->  System.out.println("Invalid amount")
        );

        AccountStatement accountStatement = accountOperation.execute(account, BigDecimal.valueOf(amount), LocalDateTime.now());
        account.getAccountStatementList().add(accountStatement);
        return true;
    }
}
