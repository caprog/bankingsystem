package fr.bank.bankingsystem.hmi;

import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;

public class ScannerUtils {

    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String NUMBER_PATTERN = "[0-9]+";

    public static int nextInt(Consumer<String> formatErrorSubscribe) {
        return ScannerUtils.nextInt(formatErrorSubscribe, (unused) -> true);
    }

    public static int nextInt(Consumer<String> formatErrorSubscribe, Function<Integer, Boolean> validator) {
        Integer dataResult = null;
        do {
            String data = SCANNER.nextLine();
            if(!data.matches(NUMBER_PATTERN))
                formatErrorSubscribe.accept(data);
            else {
                int temp = Integer.parseInt(data);
                if(validator.apply(temp))
                    dataResult = temp;
            }
        }while(dataResult == null);

        return dataResult;
    }
}
