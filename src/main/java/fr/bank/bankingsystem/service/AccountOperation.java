package fr.bank.bankingsystem.service;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface AccountOperation {

    AccountStatement execute(Account account, BigDecimal amount, LocalDateTime date);
}
