package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.service.AccountStatementHistoricalPrinter;
import fr.bank.bankingsystem.domain.Account;

import java.util.Comparator;
import java.util.List;

public class AccountStatementHistoricalConsolePrinter implements AccountStatementHistoricalPrinter {

    private static AccountStatementHistoricalConsolePrinter INSTANCE;

    private AccountStatementHistoricalConsolePrinter(){}

    public static synchronized AccountStatementHistoricalConsolePrinter getInstance(){

        if(INSTANCE == null)
            INSTANCE = new AccountStatementHistoricalConsolePrinter();

        return INSTANCE;
    }

    @Override
    public void print(Account account) {
        List<AccountStatement> accountStatementList = account.getAccountStatementList();
        if(accountStatementList == null)
            System.out.println("No historical data found");
        else{
            Comparator<AccountStatement> dateDesc = (first, second) -> first.getDate().isAfter(second.getDate()) ? -1 : 1;
            accountStatementList.sort(dateDesc);
            accountStatementList.stream().forEach((statement) -> System.out.println(statement));
        }
    }
}
