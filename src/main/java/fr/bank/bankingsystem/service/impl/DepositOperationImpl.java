package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.domain.Operation;
import fr.bank.bankingsystem.service.AccountOperation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class DepositOperationImpl implements AccountOperation {

    private static DepositOperationImpl INSTANCE;

    private DepositOperationImpl(){}

    public static synchronized DepositOperationImpl getInstance(){

        if(INSTANCE == null)
            INSTANCE = new DepositOperationImpl();

        return INSTANCE;
    }

    @Override
    public AccountStatement execute(Account account, BigDecimal amount, LocalDateTime date) {

        List<AccountStatement> accountStatementList = account.getAccountStatementList();

        if(accountStatementList == null)
            return new AccountStatement(Operation.DEPOSIT, date, amount, amount);

        BigDecimal lastBalance = accountStatementList.stream()
                                    .reduce((last, current) -> last.getDate().isAfter(current.getDate()) ? last : current)
                                    .map(last -> last.getBalance())
                                    .orElseGet(() -> new BigDecimal(0));

        return new AccountStatement(Operation.DEPOSIT, date, amount, lastBalance.add(amount));
    }
}
