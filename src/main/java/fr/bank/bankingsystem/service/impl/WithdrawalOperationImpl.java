package fr.bank.bankingsystem.service.impl;

import fr.bank.bankingsystem.domain.Account;
import fr.bank.bankingsystem.domain.AccountStatement;
import fr.bank.bankingsystem.domain.Operation;
import fr.bank.bankingsystem.service.AccountOperation;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class WithdrawalOperationImpl implements AccountOperation {

    private static WithdrawalOperationImpl INSTANCE;

    private WithdrawalOperationImpl(){}

    public static synchronized WithdrawalOperationImpl getInstance(){

        if(INSTANCE == null)
            INSTANCE = new WithdrawalOperationImpl();

        return INSTANCE;
    }

    @Override
    public AccountStatement execute(Account account, BigDecimal amount, LocalDateTime date) {

        List<AccountStatement> accountStatementList = account.getAccountStatementList();

        if(accountStatementList == null)
            return null;

        BigDecimal lastBalance = accountStatementList.stream()
                                    .reduce((last, current) -> last.getDate().isAfter(current.getDate()) ? last : current)
                                    .map(last -> last.getBalance())
                                    .orElseGet(() -> new BigDecimal(0));

        if(BigDecimal.valueOf(0).equals(lastBalance))
            return null; // or throw business exception

        return new AccountStatement(Operation.WITHDRAWAL, date, amount, lastBalance.subtract(amount));
    }
}
