package fr.bank.bankingsystem.service;

import fr.bank.bankingsystem.domain.Account;

public interface AccountStatementHistoricalPrinter {
    void print(Account account);
}
