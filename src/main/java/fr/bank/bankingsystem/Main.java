package fr.bank.bankingsystem;

import fr.bank.bankingsystem.hmi.InteractiveShell;

public class Main {

    public static void main(String[] args) {
        InteractiveShell.getInstance().start();
    }
}
